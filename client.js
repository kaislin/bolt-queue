const clientapi = require('./clientapi.js');
const cluster = require('cluster');

function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function generateUuid() {
  return Math.random().toString()
        + Math.random().toString()
        + Math.random().toString();
}

(async () => {
  const reply = await clientapi.SendMSG('registory', {
    jobid: makeid(10),
  }, generateUuid())
  console.log('reply:', reply);
})();

(async () => {
  const reply = await clientapi.SendMSG('registory', {
    jobid: makeid(10),
  }, generateUuid())
  console.log('reply:', reply);
})();

(async () => {
  const reply = await clientapi.SendMSG('registory', {
    jobid: makeid(10),
  }, generateUuid())
  console.log('reply:', reply);
})();

(async () => {
  const reply = await clientapi.SendMSG('registory', {
    jobid: makeid(10),
  }, generateUuid())
  console.log('reply:', reply);
})();

(async () => {
  const reply = await clientapi.SendMSG('registory', {
    jobid: makeid(10),
  }, generateUuid())
  console.log('reply:', reply);
})();

// *** 另一種 call 法 ***
// 
// clientapi.SendMSG('registory', {
//   jobid: makeid(10),
// }, generateUuid())
//   .then((reply) => {
//     console.log('reply :', reply)
//   })
//   .catch((e) => {
//     console.log('catch:', e);
//   })

// clientapi.SendMSG('registory', {
//   jobid: makeid(10),
// }, generateUuid())
//   .then((reply) => {
//     console.log('reply :', reply)
//   })
//   .catch((e) => {
//     console.log('catch:', e);
//   })

// clientapi.SendMSG('registory', {
//   jobid: makeid(10),
// }, generateUuid())
//   .then((reply) => {
//     console.log('reply :', reply)
//   })
//   .catch((e) => {
//     console.log('catch:', e);
//   })

// clientapi.SendMSG('registory', {
//   jobid: makeid(10),
// }, generateUuid())
//   .then((reply) => {
//     console.log('reply :', reply)
//   })
//   .catch((e) => {
//     console.log('catch:', e);
//   })

// clientapi.SendMSG('registory', {
//   jobid: makeid(10),
// }, generateUuid())
//   .then((reply) => {
//     console.log('reply :', reply)
//   })
//   .catch((e) => {
//     console.log('catch:', e);
//   })



// *** 共用同一個 connection ***
// 未完成 code，需移除 clientapi.js 對應的註解才能跑
// const channel =  clientapi.CreateClient('registory')

// channel
//   .then((ch) => {
//     const correlationId = generateUuid()
//     const tmp = makeid(10)
//     clientapi.SendRPCMessage(ch,'registory', {
//       jobid: tmp,
//     })
//       .then((reply) => {
//         console.log('reply :',  reply)
//       })
//       .catch(() => {
//         console.log('catch!!');
//       })
//     return ch
//   })
//   .then((ch) => {
//     const correlationId = generateUuid()
//     const tmp = makeid(10)
//     clientapi.SendRPCMessage(ch,'registory', {
//       jobid: tmp,
//     })
//       .then((reply) => {
//         console.log('reply :',  reply)
//       })
//       .catch(() => {
//         console.log('catch!!');
//       })
//     return ch
//   })
//   .then((ch) => {
//     const correlationId = generateUuid()
//     const tmp = makeid(10)
//     clientapi.SendRPCMessage(ch,'registory', {
//       jobid: tmp,
//     })
//       .then((reply) => {
//         console.log('reply :',  reply)
//       })
//       .catch(() => {
//         console.log('catch!!');
//       })
//     return ch
//   })
//   .then((ch) => {
//     const correlationId = generateUuid()
//     const tmp = makeid(10)
//     clientapi.SendRPCMessage(ch,'registory', {
//       jobid: tmp,
//     })
//       .then((reply) => {
//         console.log('reply :',  reply)
//       })
//       .catch(() => {
//         console.log('catch!!');
//       })
//     return ch
//   })
//   .then((ch) => {
//     const correlationId = generateUuid()
//     const tmp = makeid(10)
//     clientapi.SendRPCMessage(ch,'registory', {
//       jobid: tmp,
//     })
//       .then((reply) => {
//         console.log('reply :',  reply)
//       })
//       .catch(() => {
//         console.log('catch!!');
//       })
//     return ch
//   })
