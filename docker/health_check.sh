#!/bin/bash

export PATH=/opt/rabbitmq/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

SLACK_CHANNEL="alert_test"
SLACK_HOOKS_URL="https://hooks.slack.com/services/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"


slack_push() {
  curl -X POST --data-urlencode "payload={\"channel\": \"#$SLACK_CHANNEL\", \"username\": \"bolt-rabbit\", \"text\": \"$1\", \"icon_emoji\": \":rabbit:\"}" $SLACK_HOOKS_URL
}

main() {
  PING=$(rabbitmq-diagnostics --node rabbit ping -q)
  if [ $? -ne 0 ]; then
    slack_push "ping rabbit node `error`: $PING"
  fi

  STATUS=$(rabbitmq-diagnostics --node rabbit -q status)
  if [ $? -ne 0 ]; then
    slack_push "get rabbit node status `error`: $STATUS"
  fi

  ALARMS=$(rabbitmq-diagnostics --node rabbit -q check_running && rabbitmq-diagnostics --node rabbit -q check_local_alarms)
  if [ $? -ne 0 ]; then
    slack_push "get rabbit node alarms `error`: $ALARMS"
  fi

  HEALTH=$(rabbitmq-diagnostics --node rabbit -q check_port_connectivity && rabbitmq-diagnostics --node rabbit -q node_health_check)
  if [ $? -ne 0 ]; then
    slack_push "check rabbit node port & health `error`: $HEALTH"
  fi
}

main "$@"