# deploy RabbitMQ

## rabbitmq config

- rabbitmq.conf
- .env

## health_check

health_check.sh - 檢測 rabbitmq 一些 alarms 會資訊，並在出問題時推播到指定的 slack channel

> ※ 透過 install_cron.sh 把 health_check.sh 放到 docker 內使用 crontab 做定期檢測，但可能會因為 docker 出問題而導致沒有檢測或推播

## How to use

因為有開啟 SSL

所以必須在該目錄下新增

- `key/ca_certificate_bundle.pem`
- `key/server_certificate.pem`
- `key/server_key.pem`

### create SSL CA

```
git clone https://github.com/michaelklishin/tls-gen tls-gen
cd tls-gen/basic
make PASSWORD=<PASSWORD>
cd  ./result
cat server_certificate.pem ca_certificate.pem> ca_certificate_bundle.pem
```

然後將 key 複製到相對的位置


### Run

```
docker-compose up -d
```

### stop 

```
docker-compose stop
```

### stop & delete

```
docker-compose down
```
