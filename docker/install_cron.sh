apt-get update && apt-get install cron curl -y && apt-get clean &&
chmod 0644 -R /etc/cron.d &&
crontab /etc/cron.d/health-check-cron &&
service cron start &&
cp -f /var/lib/rabbitmq/rabbitmq.conf /etc/rabbitmq/rabbitmq.conf
rabbitmq-server