# bolt-queue

## amqplib sample code

https://github.com/squaremo/amqp.node/tree/master/examples

## 單機 deploy docker

README 

```
./docker/README.md
```

## RPC Asynchronous 

- clientapi.js
- workerapi.js

### run demo

```
## 如果沒有拿到 SSL CA ，可以直接註解掉 workerapi.js 跟 clientapi.js 內的

// const opts = {
//   cert: fs.readFileSync('./docker/key/client_certificate.pem'),
//   key: fs.readFileSync('./docker/key/client_key.pem'),
//   ca: [fs.readFileSync('./docker/key/ca_certificate_bundle.pem')],
//   passphrase: 'passphrase',
// };

# cp and edit config.js
cp config.example.js config.js
# edit config
vi config.js
# start worker
node worker.js

# other terminal start
node client.js
```