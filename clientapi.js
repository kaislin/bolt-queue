// const amqp = require('amqplib/callback_api');
const amqp = require('amqplib');
const fs = require('fs');
const config = require('./config.js');
const events = require('events');

function generateUuid() {
  return Math.random().toString()
        + Math.random().toString()
        + Math.random().toString();
}

const opts = {
  cert: fs.readFileSync('./docker/key/client_certificate.pem'),
  key: fs.readFileSync('./docker/key/client_key.pem'),
  ca: [fs.readFileSync('./docker/key/ca_certificate_bundle.pem')],
  passphrase: 't0p$3kRe7',
};

// *** 共用同一個 connection ***
// 未完成 code
// const REPLY_QUEUE = 'amq.rabbitmq.reply-to';

// const CreateClient = () => {
//   console.log('start CreateClient()')
//     return amqp.connect(`${config.queIP}?heartbeat=2`, opts)
//       .then((conn) => {
//         conn.on('error', function(e) {
//           console.log('rabbitmq connect error in:', e.message);
//           setTimeout(function () {
//             console.log('restart');
//             CreateClient()
//           }, 3000);
//         })
//         return conn.createChannel();
//       })
//       .then((channel) => {
//         // create an event emitter where rpc responses will be published by correlationId
//         channel.responseEmitter = new events.EventEmitter();
//         channel.responseEmitter.setMaxListeners(0);
        
//         channel.consume(REPLY_QUEUE,
//           (msg) => channel.responseEmitter.emit(msg.properties.correlationId, JSON.parse(msg.content.toString())),

//           {
//             noAck: true,
//           });

//         return channel;
//       })
//       .catch((e) => {
//         console.log('rabbitmq connect err:', e.message)
//         setTimeout(function () {
//           CreateClient()
//         }, 3000);
//       });
// }

// const SendRPCMessage = (channel, to, message) => new Promise((resolve) => {
//   const correlationId = generateUuid();
//   channel.responseEmitter.once(correlationId, resolve);

//   channel.sendToQueue(to, Buffer.from(JSON.stringify(message)), { 
//     correlationId, 
//     replyTo: REPLY_QUEUE,
//     persistent: true,
//   });
// });



const correlationId = generateUuid();
const SendMSG = (to, data, correlationId, type = 0) => {
  // console.log('SendMSG()');

  const reuslt = amqp.connect(`${config.queIP}?heartbeat=2`, opts)
    .then((conn) => {
      process.once('SIGINT', function() { conn.close(); });

      conn.on('error', function(e) {
        
        console.log('rabbitmq connect error:', e.message);
        return new Promise((resolve, reject) => {
          return setTimeout(function () {
            const tmp = SendMSG(to, data, correlationId, 1)
            resolve(tmp)
            // return tmp
          }, 3000);
        })
      })
      return conn.createChannel()
        .then((channel) => {
          return new Promise((resolve, reject) => {
            // get reply msg
            let ok = channel.assertQueue(`${to}_receive`, {durable: true})
              .then(function(qok) { return qok.queue; });

            ok = ok
              .then(function(queue) { 
                return channel.consume(queue, (msg) => {
                  if (msg.properties.correlationId === correlationId) {
                    const result = JSON.parse(msg.content.toString());
                    console.log('in result:', result);
                    channel.ack(msg);
                    resolve(result);
                    return result
                  } else {
                    channel.nack(msg);
                  }
                })
                  .then(function() { 
                      return queue
                    });
              });
            
            if (type === 0) {
              ok = ok
                .then(function(queue) {
                  channel.sendToQueue(to, Buffer.from(JSON.stringify(data)), {
                    correlationId,
                    replyTo: queue,
                    persistent: true,
                  });
                });
            } else {
              ok = true
            }
          });
        })
        .finally((reply) => { 
          conn.close();
          return reply
        });
    })
    .catch((e) => {
      console.log('rabbitmq connect error:', e.message);
      return new Promise((resolve, reject) => {
        setTimeout(function () {
          const tmp = SendMSG(to, data, correlationId, 1)
          return tmp
        }, 3000);
      })
    });
  return reuslt
}

module.exports = {
  SendMSG,
  // CreateClient,
  // SendRPCMessage,
};
