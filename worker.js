const workerapi = require('./workerapi.js');
// const clientapi = require('./clientapi.js');
const cluster = require('cluster');

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const asyncWorker = async (data) => {
  // do something and general token
  console.log('receive data from', data);
  await sleep(3000);
  console.log('respons data', data);

  return {
    status: true,
    data: {
      originData: data,
    },
  };

  // *** send task to other service *** 
  //
  // const registData = {
  //   username: data.username,
  //   otherdata: '123',
  // };
  // const result = clientapi.SendMSG('keychain', registData);

  // const rt = result
  //   .then((msg) => {
  //     return {
  //     status: true,
  //         data: {
  //           token,
  //           keystone: msg.data.keystone,
  //           otherdata: 'test',
  //         },
  //       };
  //     })
  //     .catch((err) => {
  //       return {
  //         status: false,
  //         data: {
  //           err,
  //         },
  //       };
  //     });
  // return rt;
};


if (cluster.isMaster) {
  // main()
  for (let i = 0; i < 1; i++) {
    cluster.fork()
  }
} else {
  workerapi.ListenWorks('registory', asyncWorker);
}