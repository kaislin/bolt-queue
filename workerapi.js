const amqp = require('amqplib/callback_api');
const fs = require('fs');
const config = require('./config.js');

const isNil = (str) => {
  try {
    JSON.parse(str);
  } catch (e) {
    if (typeof str !== 'undefined') {
      return true;
    }
    return false;
  }
  return true;
};

const opts = {
  cert: fs.readFileSync('./docker/key/client_certificate.pem'),
  key: fs.readFileSync('./docker/key/client_key.pem'),
  ca: [fs.readFileSync('./docker/key/ca_certificate_bundle.pem')],
  passphrase: 't0p$3kRe7',
};


const ListenWorks = (workerName, cb) => {
  try {
    amqp.connect(`${config.queIP}?heartbeat=2`, opts, (error0, conn) => {
      if (error0) {
        console.error("conn err: ", error0.message);
        return setTimeout(function () {
          ListenWorks(workerName, cb)
        }, 1000);
      }
      conn.on('error', function(e) {
        console.log('rabbitmq connect error:', e.message);
        setTimeout(function () {
          ListenWorks(workerName, cb)
        }, 3000);
      })
      conn.createChannel((error1, channel) => {
        if (error1) {
          return setTimeout(function () {
            ListenWorks(workerName, cb)
          }, 1000);
        }

        channel.assertQueue(workerName, { durable: true });

        console.log('start listening!');

        // take only one task at a time
        channel.prefetch(1);

        channel.consume(workerName, (msg) => {
          const data = JSON.parse(msg.content);

          const reply = cb(data);

          reply.then((cbData) => {
            if (!isNil(cbData)) {
              cbData = {
                status: false,
              };
            }

            channel.ack(msg);
            
            channel.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(cbData)), {
              correlationId: msg.properties.correlationId,
              persistent: true,
            });
          });
        });
      });
    });
  } catch(e) {
    console.log('rabbitmq connect error:', e);
    
    setTimeout(function () {
      ListenWorks(workerName, cb)
    }, 3000);
  }
}

module.exports = {
  ListenWorks
};
